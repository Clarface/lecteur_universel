\babel@toc {french}{}\relax 
\babel@toc {french}{}\relax 
\contentsline {chapter}{Introduction}{2}{chapter*.2}%
\contentsline {chapter}{\numberline {1}Outils}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Matériel à disposition pour le projet}{3}{section.1.1}%
\contentsline {section}{\numberline {1.2}Logiciel fourni avec le matériel}{3}{section.1.2}%
\contentsline {section}{\numberline {1.3}Matériel et outils requis}{4}{section.1.3}%
\contentsline {chapter}{\numberline {2}Problématiques générales}{6}{chapter.2}%
\contentsline {section}{\numberline {2.1}Incompatibilité de la commande d'ouverture de porte et de la commande de pointage}{6}{section.2.1}%
\contentsline {section}{\numberline {2.2}Optimisations pour amenuiser l'utilisation de la bande passante et le coût énergétique}{7}{section.2.2}%
\contentsline {section}{\numberline {2.3}Sécurisation des protocoles}{8}{section.2.3}%
\contentsline {section}{\numberline {2.4}Des paquets inconnus}{9}{section.2.4}%
\contentsline {chapter}{\numberline {3}Première version du projet: Système de pointage pour les stagiaires}{10}{chapter.3}%
\contentsline {section}{\numberline {3.1}Description}{10}{section.3.1}%
\contentsline {section}{\numberline {3.2}Mise en place}{10}{section.3.2}%
\contentsline {chapter}{\numberline {4}Deuxième version du projet: Système d'ouverture des portes pour les stagiaires}{13}{chapter.4}%
\contentsline {section}{\numberline {4.1}Description}{13}{section.4.1}%
\contentsline {section}{\numberline {4.2}Mise en place}{13}{section.4.2}%
\contentsline {chapter}{Conclusion}{15}{chapter*.16}%
\contentsline {chapter}{Bibliographie}{16}{chapter*.17}%
