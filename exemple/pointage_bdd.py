import threading
import queue
import time
import db_traitement as db

#Ce code sera peut-être à simplifier, mais à mon sens, il est plus compréhensible de cette manière.
#A voir dans le temps

def database_thread(queue):
    
    #Connexion à la BDD  via des connexion pool
    pool = db.db_create_pool()
    
    #Boucle d'attente des données du badge
    while True:
          
        donnee = queue.get()
        
        #Si la donnee reçue est nulle, alors on ne fait rien
        if donnee != None:
            #Si la donnée reçue est un message d'arrêt du thread, on sort de la boucle infinie
            if donnee == "stop":
                break
            
            #On sait maintenant à quoi correspond la donnée recue
            numero_badge_recu = donnee
            
            #Création d'un curseur
            conn = pool.get_connection()
            conn.autocommit = True #Permet d'envoyer touts les requêtes avant la fermeture de la connexion
            cur = conn.cursor()
            
            #On vérifie que le numéro de badge est associé à un compte utilisateur, et on le recupere
            try :
                id_utilisateur = db.recup_infos_utilisateur(cur=cur,numero_badge_reçu=numero_badge_recu)
            except ValueError as e:
                print("Une erreur a été capturée : ", e)
              
            #On insère les horaires, on pointe    
            db.insertion_horaire(cur,id_utilisateur)

            # Fermeture du curseur
            cur.close()
            conn.close() #Grâce à la connexion pool, la connexion en elle même n'est pas fermée, mais la liaison à la connexion

    #Déconnexion de la plateforme
    pool.close()

    

###  FONCTION PRINCIPALE  ###

# Création une file d'attente pour la communication
donnees_envoyees_thread_bdd = queue.Queue()

#Création du thread pour l'accès à la base de données
thread_base_de_donnee = threading.Thread(target=database_thread, args=(donnees_envoyees_thread_bdd,))
thread_base_de_donnee.start()
time.sleep(1)

#Envoi des donnees
donnee = 123456789
donnees_envoyees_thread_bdd.put(donnee)
donnees_envoyees_thread_bdd.put("stop")

#Attente de la fermeture des sthreads
thread_base_de_donnee.join()
print("Communication terminée")