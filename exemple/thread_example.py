import threading
import queue
import time

# Fonction exécutée par le thread esclave
def esclave(queue):
    while True:
        # Attendre la réception de données depuis le thread principal
        donnee = queue.get()

        # Vérifier si le thread principal demande l'arrêt
        if donnee is None:
            break

        # Traiter les données
        print(f"Traitement dans le thread esclave: {donnee}")

# Créer une file d'attente pour la communication
q = queue.Queue()

# Créer et démarrer le thread esclave
thread_esclave = threading.Thread(target=esclave, args=(q,))
thread_esclave.start()

# Envoyer des données depuis le thread principal
for i in range(5):
    time.sleep(1)  # Simuler une opération longue
    donnee = f"Donnée {i}"
    print(f"Envoi depuis le thread principal: {donnee}")
    q.put(donnee)

# Signaler au thread esclave que le travail est terminé
q.put(None)

# Attendre que le thread esclave se termine
thread_esclave.join()

print("Le thread principal a terminé.")
