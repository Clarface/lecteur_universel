import socket

# Adresse IP et port de l'autre élément
dest_ip = "192.168.56.1"
dest_port = 12345

# Création d'une socket UDP
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Envoi de données à l'autre élément
data = b"Bonjour de l'element 1"
sock.sendto(data, (dest_ip, dest_port))

# Fermeture de la socket
sock.close()
