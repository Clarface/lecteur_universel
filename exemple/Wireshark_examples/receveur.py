import socket

# Adresse IP et port de cet élément
local_ip = "192.168.56.1"
local_port = 12345

# Création d'une socket UDP
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Liaison de la socket à l'adresse et au port local
sock.bind((local_ip, local_port))

# Réception des données de l'autre élément
data, addr = sock.recvfrom(30)
print(f"Reçu depuis {addr}: {data.decode('utf-8')}")

# Fermeture de la socket
sock.close()
