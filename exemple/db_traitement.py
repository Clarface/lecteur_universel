import mariadb
from datetime import datetime, date , time

conn_params = {
    "host" : "localhost",
    "user" : "pointeuse",
    "password" : "pointeusemdp",
    "database" : "horostagev2",
    "port" : 3306
}

heure_limites = {
    "arrivee_min_matin" : time(hour = 8),
    "duree_pause_min" : time(hour=1),
    "sortie_max_soir" : time(hour = 17)
}


def db_create_pool():
    #Garde une connexion constante que l'on peut réutiliser
    pool = mariadb.ConnectionPool(pool_name = "connexion_constante", pool_size = 1, **conn_params)
    return pool



def recup_infos_utilisateur(cur,numero_badge_reçu):
    cur.execute("SELECT * FROM utilisateur WHERE badge=?",
                (numero_badge_reçu,))
    results = cur.fetchall()
    nblignes = len(results)
    if nblignes != 1:
        raise ValueError("analyse_resultats_bdd.py dans verif_resultats_requete_donnee_badge, ligne 3. Le nombre de lignes récupérées n'est pas correct")
    for row in results:
        id_utilisateur = row[0]
    return id_utilisateur
    

   
def insertion_horaire(cur,utilisateur):
    # Récupération des dates actuelles
    maintenant = datetime.now()
    aujourdhui = date.today()
    #Vérification du nombre de pointages déjà présents pour cette date
    cur.execute("SELECT * FROM horaire WHERE fk_id_utilisateur=? AND date=?",
                (utilisateur, aujourdhui,))
    results = cur.fetchall()
    nblignes = len(results)
    #S'il n'y a pas encore de ligne à cette date pour cette personne, c'est que nous sommes au debut de la journée
    if nblignes == 0:
        #On rentre l'heure dans h1
        cur.execute("INSERT INTO horaire (h1, date, fk_id_utilisateur) VALUES (?, ?, ?)",(maintenant, aujourdhui,utilisateur))
    #Si il y a une ligne pour cette date et cette personne, c'est que la personne a déjà commencé sa journée
    elif nblignes == 1:
        for row in results:
            #Attention les variables contetant les heures sont des time delta
            #Un time delta est considéré comme un vecteur, il faut donc lui
            #attribuer un traitement particulier 
            id_horaire = row[0]
            h1 = row[1]
            h2 = row[2]
            h3 = row[3]
            h4 = row[4]
            
        if h2 == None:
            #On insère l'horaire dans la base de données
            cur.execute("UPDATE horaire SET h2 = ? WHERE id_horaire = ?",(maintenant,id_horaire))
            total_heure = (maintenant - h1).hour + (maintenant - h1).minute / 100
            #On vérifie que l'horaire d'arrivée est bien supérieur à l'heure minimale d'arrivée
            
            cur.execute("UPDATE horaire SET total_heure = ? WHERE id_horaire = ?",(total_heure,id_horaire))
        elif h3 == None:
            cur.execute("UPDATE horaire SET h3 = ? WHERE id_horaire = ?",(maintenant,id_horaire))
        elif h4 == None:
            cur.execute("UPDATE horaire SET h4 = ? WHERE id_horaire = ?",(maintenant,id_horaire))
            h2 = datetime.strptime(str(h2), "%H:%M:%S") #Conversion du format timdelta au format datetime
            total_minutes = (h2 - h1 ).minute / 100 + (maintenant - h3).minute / 100
            if total_minutes >= 60:
                total_minutes = total_minutes - 60
                total_heure = 1
            total_heure = total_minutes +  (h2 - h1).hour +  (maintenant - h3).hour
            cur.execute("UPDATE horaire SET total_heure = ? WHERE id_horaire = ?",(total_heure,id_horaire))
            
            #   A faire   #
            # Mettre en place le calcul du total des heures retenues
            
        else:
            print("Houston, on a un problème")
    
    
    
    
